-- Livox Main Data Stream Protocol for RS16
livox_sdk2_proto = Proto("LivoxSDK2","Livox SDK2 Stream Protocol")

function livox_sdk2_proto.dissector(buffer,pinfo,tree)
	pinfo.cols.protocol = "LivoxSDK2"

	local subtree = tree:add(livox_sdk2_proto,buffer(),"Livox SDK2 Stream Protocol:" ..buffer:len())

	local curr = 0

    -- Packet Header --
    local headerSize = 36
	local header_subtree = subtree:add(buffer(curr,headerSize),"Header")

	-- curr = curr + 2

    local versionSize = 1
	local Version = buffer(curr, versionSize):le_uint()
	header_subtree:add(buffer(curr, versionSize),"Version : " .. Version)
	curr = curr + versionSize

    local lengthSize = 2
	local Length = buffer(curr, lengthSize):le_uint()
	header_subtree:add(buffer(curr, lengthSize),"Length : " .. Length)
	curr = curr + lengthSize

    local timeIntervalSize = 2
	local TimeInterval = buffer(curr, timeIntervalSize):le_uint()
	header_subtree:add(buffer(curr, timeIntervalSize),"TimeInterval : " .. TimeInterval)
	curr = curr + timeIntervalSize

    local dotNumSize = 2
	local DotNum = buffer(curr, dotNumSize):le_uint()
	header_subtree:add(buffer(curr, dotNumSize),"DotNum : " .. DotNum)
	curr = curr + dotNumSize

    local udpCountSize = 2
	local UDPCount = buffer(curr, udpCountSize):le_uint()
	header_subtree:add(buffer(curr, udpCountSize),"UDPCount : " .. UDPCount)
	curr = curr + udpCountSize

    local frameCountSize = 1
	local FrameCount = buffer(curr, frameCountSize):le_uint()
	header_subtree:add(buffer(curr, frameCountSize),"FrameCount : " .. FrameCount)
	curr = curr + frameCountSize

    -- Reserved
    curr = curr + 18

    local timestampSize = 8
	local TimeStamp = buffer(curr, timestampSize):le_uint64()
	header_subtree:add(buffer(curr, timestampSize),"TimeStamp : " .. TimeStamp)
	curr = curr + timestampSize

	---- Data Blocks ----

	-- local nbDataBlock = 12
	-- local sizeDataBlock = 100
	-- local nbChannel = 32
    -- local channelSize = 3
	-- local DataBlockSize = nbDataBlock * nbChannel * channelSize + 4

	-- local datablocks = subtree:add(buffer(curr, DataBlockSize),"Data blocks")

	-- for i=1, nbDataBlock
	-- do
	-- 	local dataBlock_subtree = datablocks:add(buffer(curr, sizeDataBlock),"Data Block " ..i)

    --     -- Flag always 0xff, 0xee
	-- 	curr = curr + 2

	-- 	local Azimuth = buffer(curr,2):uint()
	-- 	dataBlock_subtree:add(buffer(curr,2),"Azimuth  : " .. Azimuth)
	-- 	curr = curr + 2
	-- end
end


-- load the udp.port table
udp_table = DissectorTable.get("udp.port")
-- register our protocol to handle udp port
udp_table:add(56301,livox_sdk2_proto)
