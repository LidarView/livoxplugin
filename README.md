# Livox Plugin

This plugin wrap the [livox sdk 2](https://github.com/Livox-SDK/Livox-SDK2) in a ParaView plugin. This allow LidarView to be able to read Livox LiDARs.

# License

This repository is under the Apache 2.0 license, see NOTICE and LICENSE file.
