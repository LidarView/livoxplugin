/*=========================================================================

  Program: LidarView
  Module:  TestLivoxLidarStream.cxx

  Copyright (c) Kitware Inc.
  All rights reserved.
  See LICENSE or http://www.apache.org/licenses/LICENSE-2.0 for details.

  This software is distributed WITHOUT ANY WARRANTY; without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "TestLivoxInfo.h"

#include "vtkLidarStream.h"
#include "vtkLidarTestTools.h"
#include "vtkLivoxPacketInterpreter.h"

#include <vtkNew.h>
#include <vtkTestUtilities.h>

#include <string>

//----------------------------------------------------------------------------
int TestLivoxLidarStream(int argc, char* argv[])
{
  for (unsigned int idx = 0; idx < TestLivoxInfo::MODELS_NB; idx++)
  {
    const char* modelName = TestLivoxInfo::LIDAR_MODELS[idx];

    std::cout << "\n=========================\n" << std::endl;
    std::cout << "Testing LidarStream for Livox-" << modelName << " model" << std::endl;

    std::string folderName = vtkTestUtilities::ExpandDataFileName(argc, argv, modelName);
    std::string pcapFileName = folderName + "/" + modelName + ".pcap";
    std::string referenceDataXML = folderName + "/" + modelName + "-reference-data.xml";

    vtkNew<vtkLidarStream> stream;
    vtkNew<vtkLivoxPacketInterpreter> interp;
    interp->SetIgnoreEmptyFrames(true);
    interp->SetIgnoreZeroDistances(true);
    interp->SetEnableAdvancedArrays(true);
    interp->SetLidarModel(TestLivoxInfo::MODEL_IDX[idx]);
    stream->SetLidarInterpreter(interp);
    stream->SetListeningPort(TestLivoxInfo::LIDAR_PORT[idx]);
    stream->SetIsCrashAnalysing(false);

    if (vtkLidarTestTools::TestLidarStreamWithBaseline(stream, pcapFileName, referenceDataXML))
    {
      std::cerr << "LidarStream baseline comparaison failed for: " << modelName << " model!"
                << std::endl;
      return EXIT_FAILURE;
    }
  }

  return EXIT_SUCCESS;
}
