/*=========================================================================

  Program: LidarView
  Module:  TestLivoxInfo.h

  Copyright (c) Kitware Inc.
  All rights reserved.
  See LICENSE or http://www.apache.org/licenses/LICENSE-2.0 for details.

  This software is distributed WITHOUT ANY WARRANTY; without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#ifndef TestLivoxInfo_h
#define TestLivoxInfo_h

namespace TestLivoxInfo
{
constexpr unsigned int MODELS_NB = 2;
constexpr const char* LIDAR_MODELS[MODELS_NB] = { "Mid360", "HAP" };
constexpr unsigned int LIDAR_PORT[MODELS_NB] = { 56301, 57000 };
constexpr unsigned int MODEL_IDX[MODELS_NB] = { 5, 6 };
};

#endif
