/*=========================================================================

  Program: LidarView
  Module:  TestLivoxLidarReader.cxx

  Copyright (c) Kitware Inc.
  All rights reserved.
  See LICENSE or http://www.apache.org/licenses/LICENSE-2.0 for details.

  This software is distributed WITHOUT ANY WARRANTY; without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "TestLivoxInfo.h"

#include "vtkLidarReader.h"
#include "vtkLidarTestTools.h"
#include "vtkLivoxPacketInterpreter.h"

#include <vtkNew.h>
#include <vtkTestUtilities.h>

#include <string>

//----------------------------------------------------------------------------
int TestLivoxLidarReader(int argc, char* argv[])
{
  for (unsigned int idx = 0; idx < TestLivoxInfo::MODELS_NB; idx++)
  {
    const char* modelName = TestLivoxInfo::LIDAR_MODELS[idx];

    std::cout << "\n=========================\n" << std::endl;
    std::cout << "Testing LidarReader for Livox-" << modelName << " model" << std::endl;

    std::string folderName = vtkTestUtilities::ExpandDataFileName(argc, argv, modelName);
    std::string pcapFileName = folderName + "/" + modelName + ".pcap";
    std::string referenceDataXML = folderName + "/" + modelName + "-reference-data.xml";

    vtkNew<vtkLidarReader> reader;
    vtkNew<vtkLivoxPacketInterpreter> interp;
    interp->SetIgnoreEmptyFrames(true);
    interp->SetIgnoreZeroDistances(true);
    interp->SetEnableAdvancedArrays(true);
    interp->SetLidarModel(TestLivoxInfo::MODEL_IDX[idx]);
    reader->SetLidarInterpreter(interp);
    reader->SetShowPartialFrames(true);
    reader->SetLidarPort(TestLivoxInfo::LIDAR_PORT[idx]);
    reader->SetFileName(pcapFileName);

    if (vtkLidarTestTools::TestLidarReaderWithBaseline(reader, referenceDataXML))
    {
      std::cerr << "LidarReader baseline comparaison failed for: " << modelName << " model!"
                << std::endl;
      return EXIT_FAILURE;
    }
  }

  return EXIT_SUCCESS;
}
