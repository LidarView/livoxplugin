/*=========================================================================

  Program:   LidarView
  Module:    vtkLivoxPacketInterpreter.h

  Copyright (c) Kitware Inc.
  All rights reserved.
  See LICENSE or http://www.apache.org/licenses/LICENSE-2.0 for details.

  This software is distributed WITHOUT ANY WARRANTY; without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#ifndef vtkLivoxPacketInterpreter_h
#define vtkLivoxPacketInterpreter_h

#include <vtkLidarPacketInterpreter.h>

#include <memory>
#include <vtkSetGet.h>

#include "LivoxPacketInterpretersModule.h"

class LIVOXPACKETINTERPRETERS_EXPORT vtkLivoxPacketInterpreter : public vtkLidarPacketInterpreter
{
public:
  static vtkLivoxPacketInterpreter* New();
  vtkTypeMacro(vtkLivoxPacketInterpreter, vtkLidarPacketInterpreter);
  void PrintSelf(ostream& vtkNotUsed(os), vtkIndent vtkNotUsed(indent)) override;

  void Initialize() override;

  void ProcessPacket(unsigned char const* data, unsigned int dataLength) override;

  bool IsLidarPacket(unsigned char const* data, unsigned int dataLength) override;

  bool PreProcessPacket(unsigned char const* data,
    unsigned int dataLength,
    double& outLidarDataTime) override;

  vtkGetMacro(PublishInterval, long);
  vtkSetAndResetInitializeMacro(PublishInterval, long);

  void SetLidarModel(int type);
  int GetLidarModel();

protected:
  vtkSmartPointer<vtkPolyData> CreateNewEmptyFrame(vtkIdType nbrOfPoints,
    vtkIdType prereservedNbrOfPoints = 60000) override;

  vtkLivoxPacketInterpreter();
  ~vtkLivoxPacketInterpreter() = default;

private:
  vtkLivoxPacketInterpreter(const vtkLivoxPacketInterpreter&) = delete;
  void operator=(const vtkLivoxPacketInterpreter&) = delete;

  long PublishInterval = 100; // in ms

  class vtkInternals;
  std::unique_ptr<vtkInternals> Internals;
};

#endif // vtkLivoxPacketInterpreter_h
