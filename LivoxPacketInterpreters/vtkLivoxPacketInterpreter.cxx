/*=========================================================================

  Program:   LidarView
  Module:    vtkLivoxPacketInterpreter.cxx

  Copyright (c) Kitware Inc.
  All rights reserved.
  See LICENSE or http://www.apache.org/licenses/LICENSE-2.0 for details.

  This software is distributed WITHOUT ANY WARRANTY; without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "vtkLivoxPacketInterpreter.h"

#include "InterpreterHelper.h"
#include "LivoxPacketFormat.h"

#include <vtkDoubleArray.h>
#include <vtkPointData.h>
#include <vtkPoints.h>

#include <vtkUnsignedShortArray.h>

#include <cmath>

namespace
{
typedef livox::sdk1::EthernetPacket SDK1Packet;
typedef livox::sdk2::EthernetPacket SDK2Packet;
}

//-----------------------------------------------------------------------------
class vtkLivoxPacketInterpreter::vtkInternals
{
public:
  //-----------------------------------------------------------------------------
  template <typename PacketT>
  bool DoSplitPacket(unsigned char const* data, uint64_t interval)
  {
    const PacketT* pkt = reinterpret_cast<const PacketT*>(data);
    double timestamp = livox::GetEthPacketTimestamp(pkt->timestamp, 8);
    if (this->IsFirstTime || this->LastTimeStamp > timestamp ||
      timestamp - this->LastTimeStamp > interval * 2U)
    {
      this->IsFirstTime = false;
      this->LastTimeStamp = timestamp;
      return false;
    }

    if (timestamp - this->LastTimeStamp < interval)
    {
      return false;
    }
    this->LastTimeStamp = timestamp;
    return true;
  }

  //-----------------------------------------------------------------------------
  template <typename PacketT>
  bool PreProcessPacket(unsigned char const* data, uint64_t interval, double& outLidarDataTime)
  {
    const PacketT* pkt = reinterpret_cast<const PacketT*>(data);
    outLidarDataTime = livox::GetEthPacketTimestamp(pkt->timestamp, 8) * ::MS_TO_S_FACTOR;
    return this->DoSplitPacket<PacketT>(data, interval);
  }

  //-----------------------------------------------------------------------------
  void ProcessSDK1Packet(unsigned char const* data)
  {
    const SDK1Packet* pkt = reinterpret_cast<const SDK1Packet*>(data);
    livox::LidarPoint point;
    point.type = this->LidarModel;
    for (uint32_t idx = 0; idx < livox::sdk1::NB_POINTS_PER_PACKETS[pkt->dataType]; idx++)
    {
      point.laserId = this->GetLaserId(idx);
      for (uint16_t iRet = 0; iRet < livox::sdk1::NUMBER_OF_RETURNS[pkt->dataType]; iRet++)
      {
        livox::ParsePoint(pkt, point, idx, iRet);
        this->FillPoints(point);
      }
    }
  }

  //-----------------------------------------------------------------------------
  void ProcessSDK2Packet(unsigned char const* data)
  {
    const SDK2Packet* pkt = reinterpret_cast<const SDK2Packet*>(data);
    livox::LidarPoint point;
    point.type = this->LidarModel;
    for (uint32_t idx = 0; idx < pkt->dotNum; idx++)
    {
      point.laserId = this->GetLaserId(idx);
      livox::ParsePoint(pkt, point, idx);
      this->FillPoints(point);
    }
  }

  //-----------------------------------------------------------------------------
  const char* GetTagName(uint8_t tagIdx)
  {
    if (tagIdx >= 3)
    {
      return "";
    }
    switch (this->LidarModel)
    {
      case livox::MID_360:
        return livox::sdk2::MID360_TAG_NAMES[tagIdx];

      default:
        return livox::DEFAULT_TAG_NAMES[tagIdx];
    }
  }

private:
  //-----------------------------------------------------------------------------
  void FillPoints(const livox::LidarPoint& point)
  {
    if (std::isnan(point.position[0]) || std::isnan(point.position[1]) ||
      std::isnan(point.position[2]))
    {
      return;
    }

    double distance = std::sqrt(std::pow(0 - point.position[0], 2) +
      std::pow(0 - point.position[1], 2) + std::pow(0 - point.position[2], 2));

    // Skip wrong points
    if (distance <= 0.1 || distance > 500.0)
    {
      return;
    }

    this->Points->InsertNextPoint(point.position);
    InsertNextValueIfNotNull(this->PointsX, point.position[0]);
    InsertNextValueIfNotNull(this->PointsY, point.position[1]);
    InsertNextValueIfNotNull(this->PointsZ, point.position[2]);
    InsertNextValueIfNotNull(this->Intensity, point.reflectivity);
    InsertNextValueIfNotNull(this->LaserId, point.laserId);
    InsertNextValueIfNotNull(this->Timestamp, point.timestamp);
    InsertNextValueIfNotNull(this->Distance, distance);

    if (this->LidarModel != livox::MID_40)
    {
      InsertNextValueIfNotNull(this->FirstTag, BF_GET(point.tag, 0, 2));
      InsertNextValueIfNotNull(this->SecondTag, BF_GET(point.tag, 2, 2));
    }
    if (this->LidarModel != livox::MID_40 && this->LidarModel != livox::HAP)
    {
      InsertNextValueIfNotNull(this->ThirdTag, BF_GET(point.tag, 4, 2));
    }
  }

  //-----------------------------------------------------------------------------
  uint8_t GetLaserId(uint32_t pointIdx)
  {
    return pointIdx % livox::NB_LASER_PER_DEVICE[this->LidarModel];
  }

public:
  bool IsFirstTime = true;
  double LastTimeStamp;

  livox::DeviceType LidarModel = livox::MID_360;

  vtkSmartPointer<vtkPoints> Points;
  vtkSmartPointer<vtkDoubleArray> PointsX;
  vtkSmartPointer<vtkDoubleArray> PointsY;
  vtkSmartPointer<vtkDoubleArray> PointsZ;
  vtkSmartPointer<vtkUnsignedCharArray> Intensity;
  vtkSmartPointer<vtkUnsignedCharArray> LaserId;
  vtkSmartPointer<vtkDoubleArray> Distance;
  vtkSmartPointer<vtkDoubleArray> Timestamp;
  vtkSmartPointer<vtkUnsignedShortArray> FirstTag;
  vtkSmartPointer<vtkUnsignedShortArray> SecondTag;
  vtkSmartPointer<vtkUnsignedShortArray> ThirdTag;
};

//-----------------------------------------------------------------------------
vtkStandardNewMacro(vtkLivoxPacketInterpreter)

//-----------------------------------------------------------------------------
vtkLivoxPacketInterpreter::vtkLivoxPacketInterpreter()
  : Internals(new vtkLivoxPacketInterpreter::vtkInternals())
{
  this->ResetCurrentFrame();
  this->SetSensorVendor("Livox");
  this->SetSensorModelName(GetLidarModelName(this->Internals->LidarModel));
}

//-----------------------------------------------------------------------------
bool vtkLivoxPacketInterpreter::IsLidarPacket(unsigned char const* data, unsigned int dataLength)
{
  switch (livox::GetSDKType(this->Internals->LidarModel))
  {
    case livox::SDK1:
      return livox::IsValidPacket(reinterpret_cast<const SDK1Packet*>(data), dataLength);

    case livox::SDK2:
      return livox::IsValidPacket(reinterpret_cast<const SDK2Packet*>(data), dataLength);
  }
  return false;
}

//-----------------------------------------------------------------------------
vtkSmartPointer<vtkPolyData> vtkLivoxPacketInterpreter::CreateNewEmptyFrame(vtkIdType nbrOfPoints,
  vtkIdType prereservedNbrOfPoints)
{
  auto& internals = this->Internals;
  const int defaultPrereservedNbrOfPointsPerFrame = 60000;
  // prereserve for 50% points more than actually received in previous frame
  prereservedNbrOfPoints =
    std::max(static_cast<int>(prereservedNbrOfPoints * 1.5), defaultPrereservedNbrOfPointsPerFrame);

  vtkSmartPointer<vtkPolyData> polyData = vtkSmartPointer<vtkPolyData>::New();

  vtkNew<vtkPoints> points;
  points->SetDataTypeToFloat();
  points->Allocate(prereservedNbrOfPoints);
  if (nbrOfPoints > 0)
  {
    points->SetNumberOfPoints(nbrOfPoints);
  }
  points->GetData()->SetName("Points");

  polyData->SetPoints(points.GetPointer());

  internals->Points = points.GetPointer();
  // clang-format off
  InitArrayForPolyData(true, internals->PointsX, "X", nbrOfPoints, prereservedNbrOfPoints, polyData, this->EnableAdvancedArrays);
  InitArrayForPolyData(true, internals->PointsY, "Y", nbrOfPoints, prereservedNbrOfPoints, polyData, this->EnableAdvancedArrays);
  InitArrayForPolyData(true, internals->PointsZ, "Z", nbrOfPoints, prereservedNbrOfPoints, polyData, this->EnableAdvancedArrays);
  InitArrayForPolyData(false, internals->Intensity, "intensity", nbrOfPoints, prereservedNbrOfPoints, polyData);
  InitArrayForPolyData(false, internals->Timestamp, "timestamp", nbrOfPoints, prereservedNbrOfPoints, polyData);
  InitArrayForPolyData(false, internals->LaserId, "laser_id", nbrOfPoints, prereservedNbrOfPoints, polyData);
  InitArrayForPolyData(false, internals->Distance, "distance_m", nbrOfPoints, prereservedNbrOfPoints, polyData);
  if (internals->LidarModel != livox::MID_40)
  {
    InitArrayForPolyData(true, internals->FirstTag, internals->GetTagName(0), nbrOfPoints, prereservedNbrOfPoints, polyData, this->EnableAdvancedArrays);
    InitArrayForPolyData(true, internals->SecondTag, internals->GetTagName(1), nbrOfPoints, prereservedNbrOfPoints, polyData, this->EnableAdvancedArrays);
  }
  if (internals->LidarModel != livox::MID_40 && internals->LidarModel != livox::HAP)
  {
    InitArrayForPolyData(true, internals->ThirdTag, internals->GetTagName(2), nbrOfPoints, prereservedNbrOfPoints, polyData, this->EnableAdvancedArrays);
  }
  // clang-format on

  // Set the default array to display in the application
  polyData->GetPointData()->SetActiveScalars("intensity");
  return polyData;
}

//-----------------------------------------------------------------------------
bool vtkLivoxPacketInterpreter::PreProcessPacket(unsigned char const* data,
  unsigned int vtkNotUsed(dataLength),
  double& outLidarDataTime)
{
  auto& internals = this->Internals;
  switch (livox::GetSDKType(this->Internals->LidarModel))
  {
    case livox::SDK1:
      return internals->PreProcessPacket<SDK1Packet>(data, this->PublishInterval, outLidarDataTime);

    case livox::SDK2:
      return internals->PreProcessPacket<SDK2Packet>(data, this->PublishInterval, outLidarDataTime);
  }
  return false;
}

//-----------------------------------------------------------------------------
void vtkLivoxPacketInterpreter::ProcessPacket(unsigned char const* data,
  unsigned int vtkNotUsed(dataLength))
{
  auto& internals = this->Internals;

  // Split here to not treat twice the same packet
  bool doSplit = false;
  switch (livox::GetSDKType(this->Internals->LidarModel))
  {
    case livox::SDK1:
      doSplit = internals->DoSplitPacket<SDK1Packet>(data, this->PublishInterval);
      break;

    case livox::SDK2:
      doSplit = internals->DoSplitPacket<SDK2Packet>(data, this->PublishInterval);
      break;
  }
  if (doSplit)
  {
    Superclass::SplitFrame();
  }

  switch (livox::GetSDKType(this->Internals->LidarModel))
  {
    case livox::SDK1:
      internals->ProcessSDK1Packet(data);
      break;

    case livox::SDK2:
      internals->ProcessSDK2Packet(data);
      break;
  }
}

//-----------------------------------------------------------------------------
void vtkLivoxPacketInterpreter::SetLidarModel(int model)
{
  if (static_cast<int>(this->Internals->LidarModel) == model)
  {
    return;
  }

  auto deviceType = static_cast<livox::DeviceType>(model);
  this->Internals->LidarModel = deviceType;
  this->SetSensorModelName(livox::GetLidarModelName(deviceType));
  this->Modified();

  this->ResetInitializedState();
}

//-----------------------------------------------------------------------------
int vtkLivoxPacketInterpreter::GetLidarModel()
{
  return static_cast<int>(this->Internals->LidarModel);
}

//-----------------------------------------------------------------------------
void vtkLivoxPacketInterpreter::Initialize()
{
  this->Internals->IsFirstTime = true;
  Superclass::Initialize();
}

//----------------------------------------------------------------------------
void vtkLivoxPacketInterpreter::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
}
