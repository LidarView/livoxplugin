/*=========================================================================

  Program:   LidarView
  Module:    vtkLivoxPosePacketInterpreter.cxx

  Copyright (c) Kitware Inc.
  All rights reserved.
  See LICENSE or http://www.apache.org/licenses/LICENSE-2.0 for details.

  This software is distributed WITHOUT ANY WARRANTY; without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "vtkLivoxPosePacketInterpreter.h"

#include "InterpreterHelper.h"
#include "LivoxPacketFormat.h"

#include <vtkDoubleArray.h>
#include <vtkFloatArray.h>
#include <vtkSmartPointer.h>

namespace
{
typedef livox::sdk1::EthernetPacket SDK1Packet;
typedef livox::sdk2::EthernetPacket SDK2Packet;
}

//-----------------------------------------------------------------------------
class vtkLivoxPosePacketInterpreter::vtkInternals
{
public:
  //-----------------------------------------------------------------------------
  void InsertIMUData(const uint8_t* data, uint32_t idx, double ptsTimestamp)
  {
    const auto* imuData = reinterpret_cast<const livox::ImuRawPoint*>(data);
    this->GyroX->InsertNextValue(imuData[idx].gyroX);
    this->GyroY->InsertNextValue(imuData[idx].gyroY);
    this->GyroZ->InsertNextValue(imuData[idx].gyroZ);
    this->AccelX->InsertNextValue(imuData[idx].accX);
    this->AccelY->InsertNextValue(imuData[idx].accY);
    this->AccelZ->InsertNextValue(imuData[idx].accZ);
    this->Timestamp->InsertNextValue(ptsTimestamp);
  }

  //-----------------------------------------------------------------------------
  void ProcessSDK1Packet(unsigned char const* data)
  {
    const SDK1Packet* pkt = reinterpret_cast<const SDK1Packet*>(data);
    uint64_t ptsTimestamp = livox::GetEthPacketTimestamp(pkt->timestamp, 8);
    this->InsertIMUData(pkt->data, 0, ptsTimestamp);
  }

  //-----------------------------------------------------------------------------
  void ProcessSDK2Packet(unsigned char const* data)
  {
    const SDK2Packet* pkt = reinterpret_cast<const SDK2Packet*>(data);
    for (uint32_t idx = 0; idx < pkt->dotNum; idx++)
    {
      uint64_t ptsTimestamp =
        livox::GetEthPacketTimestamp(pkt->timestamp, 8) + idx * pkt->timeInterval;
      this->InsertIMUData(pkt->data, idx, ptsTimestamp);
    }
  }

public:
  vtkSmartPointer<vtkFloatArray> GyroX;
  vtkSmartPointer<vtkFloatArray> GyroY;
  vtkSmartPointer<vtkFloatArray> GyroZ;
  vtkSmartPointer<vtkFloatArray> AccelX;
  vtkSmartPointer<vtkFloatArray> AccelY;
  vtkSmartPointer<vtkFloatArray> AccelZ;
  vtkSmartPointer<vtkDoubleArray> Timestamp;
};

//-----------------------------------------------------------------------------
vtkStandardNewMacro(vtkLivoxPosePacketInterpreter)

//-----------------------------------------------------------------------------
vtkLivoxPosePacketInterpreter::vtkLivoxPosePacketInterpreter()
  : Internals(new vtkLivoxPosePacketInterpreter::vtkInternals())

{
  this->ResetCurrentData();
}

//----------------------------------------------------------------------------
void vtkLivoxPosePacketInterpreter::InitArrays()
{
  auto& internals = this->Internals;

  AddArrayToTableIfNotNull(internals->GyroX, this->RawInformation, "gyro_x");
  AddArrayToTableIfNotNull(internals->GyroY, this->RawInformation, "gyro_y");
  AddArrayToTableIfNotNull(internals->GyroZ, this->RawInformation, "gyro_z");
  AddArrayToTableIfNotNull(internals->AccelX, this->RawInformation, "accel_x");
  AddArrayToTableIfNotNull(internals->AccelY, this->RawInformation, "accel_y");
  AddArrayToTableIfNotNull(internals->AccelZ, this->RawInformation, "accel_z");
  AddArrayToTableIfNotNull(internals->Timestamp, this->RawInformation, "timestamp");
}

//----------------------------------------------------------------------------
bool vtkLivoxPosePacketInterpreter::IsValidPacket(unsigned char const* data,
  unsigned int dataLength)
{
  // We cannot determine which sdk is currently use - must test for both
  int ret = 0;
  ret += livox::IsValidPacket(reinterpret_cast<const SDK1Packet*>(data), dataLength, true);
  ret += livox::IsValidPacket(reinterpret_cast<const SDK2Packet*>(data), dataLength, true);
  return !!ret;
}

//----------------------------------------------------------------------------
void vtkLivoxPosePacketInterpreter::ProcessPacket(unsigned char const* data,
  unsigned int dataLength)
{
  auto& internal = this->Internals;

  if (livox::IsValidPacket(reinterpret_cast<const SDK1Packet*>(data), dataLength, true))
  {
    internal->ProcessSDK1Packet(data);
  }
  else if (livox::IsValidPacket(reinterpret_cast<const SDK2Packet*>(data), dataLength, true))
  {
    internal->ProcessSDK2Packet(data);
  }
}

void vtkLivoxPosePacketInterpreter::FillInterpolatorFromPose() {}
