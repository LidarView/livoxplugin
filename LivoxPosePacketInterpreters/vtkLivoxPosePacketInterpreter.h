/*=========================================================================

  Program:   LidarView
  Module:    vtkLivoxPosePacketInterpreter.h

  Copyright (c) Kitware Inc.
  All rights reserved.
  See LICENSE or http://www.apache.org/licenses/LICENSE-2.0 for details.

  This software is distributed WITHOUT ANY WARRANTY; without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#ifndef vtkLivoxPosePacketInterpreters_h
#define vtkLivoxPosePacketInterpreters_h

#include "vtkPosePacketInterpreter.h"

#include <memory>

#include "LivoxPosePacketInterpretersModule.h"

//------------------------------------------------------------------------------
class LIVOXPOSEPACKETINTERPRETERS_EXPORT vtkLivoxPosePacketInterpreter
  : public vtkPosePacketInterpreter
{
public:
  static vtkLivoxPosePacketInterpreter* New();

  vtkTypeMacro(vtkLivoxPosePacketInterpreter, vtkPosePacketInterpreter)

  vtkLivoxPosePacketInterpreter();

  bool HasRawInformation() override { return true; }
  bool HasPoseInformation() override { return false; }

  bool IsValidPacket(unsigned char const* packetData, unsigned int dataLength) override;

  void ProcessPacket(unsigned char const* packetData, unsigned int /*dataLength*/) override;

  void InitArrays() override;

  void FillInterpolatorFromPose() override;

private:
  vtkLivoxPosePacketInterpreter(const vtkLivoxPosePacketInterpreter&) = delete;
  void operator=(const vtkLivoxPosePacketInterpreter&) = delete;

  class vtkInternals;
  std::unique_ptr<vtkInternals> Internals;
};

#endif // vtkLivoxPosePacketInterpreters_h
