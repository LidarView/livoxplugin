/*=========================================================================

  Program:   LidarView
  Module:    LivoxPacketFormat.cxx

  Copyright (c) Kitware Inc.
  All rights reserved.
  See LICENSE or http://www.apache.org/licenses/LICENSE-2.0 for details.

  This software is distributed WITHOUT ANY WARRANTY; without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "LivoxPacketFormat.h"

// STD
#include <cmath>
#include <cstring>
#include <iostream>
#include <string>

namespace
{
typedef union
{
  uint8_t stamp_bytes[8];
  int64_t stamp;
} LdsStamp;

// ============================================================================
// ============================ Timestamps methods ============================
// ============================================================================

//-----------------------------------------------------------------------------
template <typename DataPointT>
double GetPointTimestamp(const livox::sdk1::EthernetPacket* pkt,
  livox::DeviceType type,
  uint32_t idx)
{
  double pktTimestamp = livox::GetEthPacketTimestamp(pkt->timestamp, 8) * ::MS_TO_US_FACTOR;
  auto sdk1Type = static_cast<livox::sdk1::DeviceType>(type);
  double ptsTimestamp = DataPointT::GetPointInterval(sdk1Type) * idx; // in microseconds (us)
  return pktTimestamp + ptsTimestamp;
}

//-----------------------------------------------------------------------------
double GetPointTimestamp(const livox::sdk2::EthernetPacket* pkt, uint32_t idx)
{
  double pktTimestamp = livox::GetEthPacketTimestamp(pkt->timestamp, 8) * ::MS_TO_US_FACTOR;
  double ptsTimestamp = (static_cast<double>(pkt->timeInterval) / pkt->dotNum) * idx * 0.1; // in ns
  return pktTimestamp + ptsTimestamp * ::NS_TO_US_FACTOR;
}

// ============================================================================
// ========================== Get point info methods ==========================
// ============================================================================

//-----------------------------------------------------------------------------
template <typename DataPointT>
void GetPointInfoNoTag(const livox::sdk1::EthernetPacket* pkt,
  livox::LidarPoint& point,
  uint32_t idx)
{
  const auto* points = reinterpret_cast<const DataPointT*>(pkt->data);
  point.reflectivity = points[idx].reflectivity;
  point.timestamp = ::GetPointTimestamp<DataPointT>(pkt, point.type, idx);
}

//-----------------------------------------------------------------------------
template <typename DataPointT>
void GetPointInfo(const livox::sdk1::EthernetPacket* pkt, livox::LidarPoint& point, uint32_t idx)
{
  const auto* points = reinterpret_cast<const DataPointT*>(pkt->data);
  point.reflectivity = points[idx].reflectivity;
  point.timestamp = ::GetPointTimestamp<DataPointT>(pkt, point.type, idx);
  point.tag = points[idx].tag;
}

//-----------------------------------------------------------------------------
template <typename DataPointT>
void GetPointInfo(const livox::sdk1::EthernetPacket* pkt,
  livox::LidarPoint& point,
  uint32_t idx,
  uint16_t iRet)
{
  const auto* points = reinterpret_cast<const DataPointT*>(pkt->data);
  point.reflectivity = points[idx].data[iRet].reflectivity;
  point.timestamp = ::GetPointTimestamp<DataPointT>(pkt, point.type, idx);
  point.tag = points[idx].data[iRet].tag;
}

//-----------------------------------------------------------------------------
template <typename DataPointT>
void GetPointInfo(const livox::sdk2::EthernetPacket* pkt, livox::LidarPoint& point, uint32_t idx)
{
  const auto* points = reinterpret_cast<const DataPointT*>(pkt->data);
  point.reflectivity = points[idx].reflectivity;
  point.tag = points[idx].tag;
  point.timestamp = ::GetPointTimestamp(pkt, idx);
}

// ============================================================================
// ======================== Get cartesian point methods =======================
// ============================================================================

//-----------------------------------------------------------------------------
template <typename DataPointT>
void GetCartesianPoint(const DataPointT& data, double pos[3], double factor)
{
  pos[0] = static_cast<double>(data.x) * factor;
  pos[1] = static_cast<double>(data.y) * factor;
  pos[2] = static_cast<double>(data.z) * factor;
}

//-----------------------------------------------------------------------------
template <typename DataPointT>
void GetCartesianPoint(const uint8_t* data,
  double pos[3],
  uint32_t idx,
  double factor = ::MM_TO_M_FACTOR)
{
  const auto* points = reinterpret_cast<const DataPointT*>(data);
  ::GetCartesianPoint(points[idx], pos, factor);
}

//-----------------------------------------------------------------------------
template <typename DataPointT>
void GetCartesianPoint(const uint8_t* data, double pos[3], uint32_t idx, uint16_t iRet)
{
  const auto* points = reinterpret_cast<const DataPointT*>(data);
  ::GetCartesianPoint(points[idx].data[iRet], pos, ::MM_TO_M_FACTOR);
}

// ============================================================================
// ======================== Get spherical point methods =======================
// ============================================================================

//-----------------------------------------------------------------------------
template <typename DataPointT>
void GetSphericalPoint(const uint8_t* data, double pos[3], uint32_t idx, uint32_t depth)
{
  const auto* points = reinterpret_cast<const DataPointT*>(data);

  double rho = static_cast<double>(depth) * ::MM_TO_M_FACTOR;
  double theta = static_cast<double>(points[idx].theta) / 100; // unit 0.01 degree
  double phi = static_cast<double>(points[idx].phi) / 100;     // unit 0.01 degree

  pos[0] = rho * std::sin(phi) * std::cos(theta);
  pos[1] = rho * std::sin(phi) * std::sin(theta);
  pos[2] = rho * std::cos(phi);
}

//-----------------------------------------------------------------------------
template <typename DataPointT>
void GetSphericalPoint(const uint8_t* data, double pos[3], uint32_t idx)
{
  const auto* points = reinterpret_cast<const DataPointT*>(data);
  ::GetSphericalPoint<DataPointT>(data, pos, idx, points[idx].depth);
}

//-----------------------------------------------------------------------------
template <typename DataPointT>
void GetSphericalPoint(const uint8_t* data, double pos[3], uint32_t idx, uint16_t iRet)
{
  const auto* points = reinterpret_cast<const DataPointT*>(data);
  ::GetSphericalPoint<DataPointT>(data, pos, idx, points[idx].data[iRet].depth);
}
}

//-----------------------------------------------------------------------------
std::string livox::GetLidarModelName(livox::DeviceType model)
{
  switch (model)
  {
    case livox::MID_40:
      return "Mid-40";
    case livox::TELE_15:
      return "Tele-15";
    case livox::HORIZON:
      return "Horizon";
    case livox::MID_70:
      return "Mid-70";
    case livox::AVIA:
      return "Avia";
    case livox::MID_360:
      return "Mid-360";
    case livox::HAP:
      return "HAP";
    default:
      return "Unsupported Model";
  }
}

//-----------------------------------------------------------------------------
livox::SDKType livox::GetSDKType(DeviceType model)
{
  return static_cast<SDKType>(model == MID_360 || model == HAP);
};

//-----------------------------------------------------------------------------
double livox::GetEthPacketTimestamp(const uint8_t* time_stamp, uint8_t size)
{
  ::LdsStamp time;
  std::memcpy(time.stamp_bytes, time_stamp, size);
  return time.stamp * ::NS_TO_MS_FACTOR;
}

//-----------------------------------------------------------------------------
bool livox::IsValidPacket(const sdk1::EthernetPacket* pkt, uint32_t dataLength, bool isIMU)
{
  if (!pkt || dataLength < sizeof(sdk1::EthernetPacket))
  {
    return false;
  }
  if (isIMU == (pkt->dataType != sdk1::IMU_DATA))
  {
    return false;
  }
  uint16_t pointSize = 0;
  switch (pkt->dataType)
  {
    case sdk1::CARTESIAN_POINT:
      pointSize = sizeof(sdk1::RawPoint);
      break;
    case sdk1::SPHERICAL_POINT:
      pointSize = sizeof(sdk1::SpherePoint);
      break;
    case sdk1::EXT_CARTESIAN_POINT:
      pointSize = sizeof(sdk1::ExtendRawPoint);
      break;
    case sdk1::EXT_SPHERICAL_POINT:
      pointSize = sizeof(sdk1::ExtendSpherePoint);
      break;
    case sdk1::EXT_CARTESIAN_POINT_DUAL:
      pointSize = sizeof(sdk1::DualExtendRawPoint);
      break;
    case sdk1::EXT_SPHERICAL_POINT_DUAL:
      pointSize = sizeof(sdk1::DualExtendSpherePoint);
      break;
    case sdk1::IMU_DATA:
      pointSize = sizeof(ImuRawPoint);
      break;
    case sdk1::EXT_CARTESIAN_POINT_TRIPLE:
      pointSize = sizeof(sdk1::TripleExtendRawPoint);
      break;
    case sdk1::EXT_SPHERICAL_POINT_TRIPLE:
      pointSize = sizeof(sdk1::TripleExtendSpherePoint);
      break;
    default:
      return false;
  }
  uint32_t theoreticalSize = pointSize * sdk1::NB_POINTS_PER_PACKETS[pkt->dataType];
  // Remove size of fake member data in EthernetPacket
  theoreticalSize += sizeof(sdk1::EthernetPacket) - sizeof(uint8_t);
  return theoreticalSize == dataLength;
}

//-----------------------------------------------------------------------------
bool livox::IsValidPacket(const sdk2::EthernetPacket* pkt, uint32_t dataLength, bool isIMU)
{
  if (!pkt || dataLength < sizeof(sdk2::EthernetPacket) || pkt->length != dataLength)
  {
    return false;
  }
  if (isIMU == (pkt->dataType != sdk2::IMU_DATA))
  {
    return false;
  }
  uint16_t pointSize = 0;
  switch (pkt->dataType)
  {
    case sdk2::IMU_DATA:
      pointSize = sizeof(ImuRawPoint);
      break;
    case sdk2::CARTESIAN_COORDINATE_HIGH:
      pointSize = sizeof(sdk2::CartesianHighRawPoint);
      break;
    case sdk2::CARTESIAN_COORDINATE_LOW:
      pointSize = sizeof(sdk2::CartesianLowRawPoint);
      break;
    case sdk2::SPHERICAL_COORDINATE:
      pointSize = sizeof(sdk2::SpherePoint);
      break;
    default:
      return false;
  }
  uint32_t theoreticalSize = pointSize * pkt->dotNum;
  // Remove size of fake member data in EthernetPacket
  theoreticalSize += sizeof(sdk2::EthernetPacket) - sizeof(uint8_t);
  return theoreticalSize == dataLength;
}

//-----------------------------------------------------------------------------
void livox::ParsePoint(const sdk1::EthernetPacket* pkt,
  LidarPoint& point,
  uint32_t idx,
  uint16_t iRet)
{
  switch (pkt->dataType)
  {
    case sdk1::CARTESIAN_POINT:
      ::GetCartesianPoint<sdk1::RawPoint>(pkt->data, point.position, idx);
      ::GetPointInfoNoTag<sdk1::RawPoint>(pkt, point, idx);
      break;

    case sdk1::SPHERICAL_POINT:
      ::GetSphericalPoint<sdk1::SpherePoint>(pkt->data, point.position, idx);
      ::GetPointInfoNoTag<sdk1::SpherePoint>(pkt, point, idx);
      break;

    case sdk1::EXT_CARTESIAN_POINT:
      ::GetCartesianPoint<sdk1::ExtendRawPoint>(pkt->data, point.position, idx);
      ::GetPointInfo<sdk1::ExtendRawPoint>(pkt, point, idx);
      break;

    case sdk1::EXT_SPHERICAL_POINT:
      ::GetSphericalPoint<sdk1::ExtendSpherePoint>(pkt->data, point.position, idx);
      ::GetPointInfo<sdk1::ExtendSpherePoint>(pkt, point, idx);
      break;

    case sdk1::EXT_CARTESIAN_POINT_DUAL:
      ::GetCartesianPoint<sdk1::DualExtendRawPoint>(pkt->data, point.position, idx, iRet);
      ::GetPointInfo<sdk1::DualExtendRawPoint>(pkt, point, idx, iRet);
      break;

    case sdk1::EXT_SPHERICAL_POINT_DUAL:
      ::GetSphericalPoint<sdk1::DualExtendSpherePoint>(pkt->data, point.position, idx, iRet);
      ::GetPointInfo<sdk1::DualExtendSpherePoint>(pkt, point, idx, iRet);
      break;

    case sdk1::EXT_CARTESIAN_POINT_TRIPLE:
      ::GetCartesianPoint<sdk1::TripleExtendRawPoint>(pkt->data, point.position, idx, iRet);
      ::GetPointInfo<sdk1::TripleExtendRawPoint>(pkt, point, idx, iRet);
      break;

    case sdk1::EXT_SPHERICAL_POINT_TRIPLE:
      ::GetSphericalPoint<sdk1::TripleExtendSpherePoint>(pkt->data, point.position, idx, iRet);
      ::GetPointInfo<sdk1::TripleExtendSpherePoint>(pkt, point, idx, iRet);
      break;
  }
}

//-----------------------------------------------------------------------------
void livox::ParsePoint(const sdk2::EthernetPacket* pkt, LidarPoint& point, uint32_t idx)
{
  switch (pkt->dataType)
  {
    case sdk2::CARTESIAN_COORDINATE_HIGH:
      ::GetCartesianPoint<sdk2::CartesianHighRawPoint>(pkt->data, point.position, idx);
      ::GetPointInfo<sdk2::CartesianHighRawPoint>(pkt, point, idx);
      break;

    case sdk2::CARTESIAN_COORDINATE_LOW:
      ::GetCartesianPoint<sdk2::CartesianLowRawPoint>(
        pkt->data, point.position, idx, ::CM_TO_M_FACTOR);
      ::GetPointInfo<sdk2::CartesianLowRawPoint>(pkt, point, idx);
      break;

    case sdk2::SPHERICAL_COORDINATE:
      ::GetSphericalPoint<sdk2::SpherePoint>(pkt->data, point.position, idx);
      ::GetPointInfo<sdk2::SpherePoint>(pkt, point, idx);
      break;
  }
}
