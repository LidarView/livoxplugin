/*=========================================================================

  Program:   LidarView
  Module:    LivoxPacketFormat.h

  Copyright (c) Kitware Inc.
  All rights reserved.
  See LICENSE or http://www.apache.org/licenses/LICENSE-2.0 for details.

  This software is distributed WITHOUT ANY WARRANTY; without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#ifndef LivoxPacketFormat_h
#define LivoxPacketFormat_h

// Local
#include "SDK1PacketFormat.h"
#include "SDK2PacketFormat.h"

// STD
#include <string>

namespace
{
constexpr double NS_TO_MS_FACTOR = 1e-6;
constexpr double MS_TO_US_FACTOR = 1e3;
constexpr double NS_TO_US_FACTOR = 1e-3;
constexpr double MS_TO_S_FACTOR = 1e-3;

constexpr double MM_TO_M_FACTOR = 1e-3;
constexpr double CM_TO_M_FACTOR = 1e-2;
}

namespace livox
{
constexpr const char* DEFAULT_TAG_NAMES[3] = { "spatial_confidence",
  "intensity_confidence",
  "return_number" };
constexpr uint8_t NB_LASER_PER_DEVICE[7] = { 1, 1, 1, 1, 6, 4, 6 };

enum DeviceType
{
  MID_40 = sdk1::MID_40,
  TELE_15 = sdk1::TELE_15,
  HORIZON = sdk1::HORIZON,
  MID_70 = sdk1::MID_70,
  AVIA = sdk1::AVIA,
  MID_360 = sdk2::MID_360,
  HAP = sdk2::HAP
};

enum SDKType
{
  SDK1 = 0,
  SDK2 = 1
};

// Instructs the compiler to pack structure members
#pragma pack(push, 1)

/** Internal representation of point information */
struct LidarPoint
{
  double position[3];
  uint8_t reflectivity;
  uint8_t tag;
  uint8_t laserId;
  double timestamp;
  DeviceType type;
};

struct ImuRawPoint
{
  float gyroX; /**< Gyroscope X axis, Unit:rad/s */
  float gyroY; /**< Gyroscope Y axis, Unit:rad/s */
  float gyroZ; /**< Gyroscope Z axis, Unit:rad/s */
  float accX;  /**< Accelerometer X axis, Unit:g */
  float accY;  /**< Accelerometer Y axis, Unit:g */
  float accZ;  /**< Accelerometer Z axis, Unit:g */
};

#pragma pack(pop)

/**
 * Return a string containing the LiDARs model name.
 */
std::string GetLidarModelName(livox::DeviceType model);

/**
 * Return the SDK used for a specific device (sdk1 or sdk2)
 */
SDKType GetSDKType(DeviceType model);

///@{
/**
 * Return true if the packet is considered valid.
 * Check if type is coherent with packet size.
 */
bool IsValidPacket(const sdk1::EthernetPacket* pkt, uint32_t dataLength, bool isIMU = false);
bool IsValidPacket(const sdk2::EthernetPacket* pkt, uint32_t dataLength, bool isIMU = false);
///@}

/**
 * Return the packet timestamp in milliseconds (ms)
 */
double GetEthPacketTimestamp(const uint8_t* time_stamp, uint8_t size);

///@{
/**
 * Methods specialized for sdk1 and sdk2 to parse point depending of the pkt data type.
 */
void ParsePoint(const sdk1::EthernetPacket* pkt, LidarPoint& point, uint32_t idx, uint16_t iRet);
void ParsePoint(const sdk2::EthernetPacket* pkt, LidarPoint& point, uint32_t idx);
///@}
};

#endif // LivoxPacketFormat_h
