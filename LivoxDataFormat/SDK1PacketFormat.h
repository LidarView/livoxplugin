/*=========================================================================

  Program:   LidarView
  Module:    SDK1PacketFormat.h

  Copyright (c) Kitware Inc.
  All rights reserved.
  See LICENSE or http://www.apache.org/licenses/LICENSE-2.0 for details.

  This software is distributed WITHOUT ANY WARRANTY; without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#ifndef SDK1PacketFormat_h
#define SDK1PacketFormat_h

#include <cstdint>

// Instructs the compiler to pack structure members
#pragma pack(push, 1)

namespace livox
{
namespace sdk1
{

enum DeviceType
{
  MID_40 = 0,
  TELE_15 = 1,
  HORIZON = 2,
  MID_70 = 3,
  AVIA = 4
};

/** Point cloud packet. */
struct EthernetPacket
{
  uint8_t version;       /**< Packet protocol version. */
  uint8_t slot;          /**< Slot number used for connecting LiDAR. */
  uint8_t id;            /**< LiDAR id. */
  uint8_t rsvd;          /**< Reserved. */
  uint32_t errCode;      /**< Device error status indicator information. */
  uint8_t timestampType; /**< Timestamp type. */
  /** Point cloud coordinate format, refer to \ref PointDataType . */
  uint8_t dataType;
  uint8_t timestamp[8]; /**< Nanosecond or UTC format timestamp. */
  uint8_t data[1];      /**< Point cloud data. */
};

constexpr double INTERVAL_100KHZ = 1. / 0.1;   // in us for 100,000 points/s
constexpr double INTERVAL_200KHZ = 1. / 0.2;   // in us for 200,000 points/s
constexpr double INTERVAL_240KHZ = 1. / 0.24;  // in us for 240,000 points/s
constexpr double INTERVAL_480KHZ = 1. / 0.48;  // in us for 480,000 points/s
constexpr double INTERVAL_720KHZ = 1. / 0.72;  // in us for 720,000 points/s
constexpr double INTERVAL_200HZ = 1. / 0.0002; // in us for IMU

/** Nb of point data per packet for each data type */
constexpr uint32_t NB_POINTS_PER_PACKETS[9] = { 100, 100, 96, 96, 48, 48, 1, 30, 30 };

/** Nb of returns for each data type */
constexpr uint16_t NUMBER_OF_RETURNS[9] = { 1, 1, 1, 1, 2, 2, 0, 3, 3 };

/** Point data type. */
enum PointDataType
{
  CARTESIAN_POINT = 0x00,            /**< Mid-40 */
  SPHERICAL_POINT = 0x01,            /**< Mid-40 */
  EXT_CARTESIAN_POINT = 0x02,        /**< Horizon, Tele-15, Mid-70, Avia */
  EXT_SPHERICAL_POINT = 0x03,        /**< Horizon, Tele-15, Mid-70, Avia */
  EXT_CARTESIAN_POINT_DUAL = 0x04,   /**< Horizon, Tele-15, Mid-70, Avia */
  EXT_SPHERICAL_POINT_DUAL = 0x05,   /**< Horizon, Tele-15, Mid-70, Avia */
  IMU_DATA = 0x06,                   /**< Horizon, Tele-15, Avia */
  EXT_CARTESIAN_POINT_TRIPLE = 0x07, /**< Avia */
  EXT_SPHERICAL_POINT_TRIPLE = 0x08, /**< Avia */
};

/** #0 Cartesian coordinate format. */
struct RawPoint
{
  int32_t x;            /**< X axis, Unit:mm */
  int32_t y;            /**< Y axis, Unit:mm */
  int32_t z;            /**< Z axis, Unit:mm */
  uint8_t reflectivity; /**< Reflectivity */

  static double GetPointInterval(DeviceType) { return INTERVAL_100KHZ; };
};

/** #1 Spherical coordinate format. */
struct SpherePoint
{
  uint32_t depth;       /**< Depth, Unit: mm */
  uint16_t theta;       /**< Zenith angle[0, 18000], Unit: 0.01 degree */
  uint16_t phi;         /**< Azimuth[0, 36000], Unit: 0.01 degree */
  uint8_t reflectivity; /**< Reflectivity */

  static double GetPointInterval(DeviceType) { return INTERVAL_100KHZ; };
};

/** #2 Extend cartesian coordinate format. */
struct ExtendRawPoint
{
  int32_t x;            /**< X axis, Unit:mm */
  int32_t y;            /**< Y axis, Unit:mm */
  int32_t z;            /**< Z axis, Unit:mm */
  uint8_t reflectivity; /**< Reflectivity */
  uint8_t tag;          /**< Tag */

  static double GetPointInterval(DeviceType type)
  {
    return type == MID_70 ? INTERVAL_100KHZ : INTERVAL_240KHZ;
  };
};

/** #3 Extend spherical coordinate format. */
struct ExtendSpherePoint
{
  uint32_t depth;       /**< Depth, Unit: mm */
  uint16_t theta;       /**< Zenith angle[0, 18000], Unit: 0.01 degree */
  uint16_t phi;         /**< Azimuth[0, 36000], Unit: 0.01 degree */
  uint8_t reflectivity; /**< Reflectivity */
  uint8_t tag;          /**< Tag */

  static double GetPointInterval(DeviceType type)
  {
    return type == MID_70 ? INTERVAL_100KHZ : INTERVAL_240KHZ;
  };
};

/** #4 Dual extend cartesian coordinate format. */
struct DualExtendRawPoint
{
  ExtendRawPoint data[2];

  static double GetPointInterval(DeviceType type)
  {
    return type == MID_70 ? INTERVAL_200KHZ : INTERVAL_480KHZ;
  };
};

/** Sphere data for dual and triple points */
struct SphereData
{
  uint32_t depth;       /**< Depth, Unit: mm */
  uint8_t reflectivity; /**< Reflectivity */
  uint8_t tag;          /**< Tag */
};

/** #5 Dual extend spherical coordinate format. */
struct DualExtendSpherePoint
{
  uint16_t theta;     /**< Zenith angle[0, 18000], Unit: 0.01 degree */
  uint16_t phi;       /**< Azimuth[0, 36000], Unit: 0.01 degree */
  SphereData data[2]; /**< See SphereData struct  */

  static double GetPointInterval(DeviceType type)
  {
    return type == MID_70 ? INTERVAL_200KHZ : INTERVAL_480KHZ;
  };
};

/** #7 Triple extend cartesian coordinate format. */
struct TripleExtendRawPoint
{
  ExtendRawPoint data[3];

  static double GetPointInterval(DeviceType) { return INTERVAL_720KHZ; };
};

/** #8 Triple extend spherical coordinate format. */
struct TripleExtendSpherePoint
{
  uint16_t theta;     /**< Zenith angle[0, 18000], Unit: 0.01 degree */
  uint16_t phi;       /**< Azimuth[0, 36000], Unit: 0.01 degree */
  SphereData data[3]; /**< See SphereData struct  */

  static double GetPointInterval(DeviceType) { return INTERVAL_720KHZ; };
};

};
};

#pragma pack(pop)

#endif // SDK1PacketFormat_h
