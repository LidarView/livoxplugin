/*=========================================================================

  Program:   LidarView
  Module:    SDK2PacketFormat.h

  Copyright (c) Kitware Inc.
  All rights reserved.
  See LICENSE or http://www.apache.org/licenses/LICENSE-2.0 for details.

  This software is distributed WITHOUT ANY WARRANTY; without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#ifndef SDK2PacketFormat_h
#define SDK2PacketFormat_h

#include <cstdint>

// Instructs the compiler to pack structure members
#pragma pack(push, 1)

namespace livox
{
namespace sdk2
{

enum DeviceType
{
  MID_360 = 5,
  HAP = 6
};

constexpr const char* MID360_TAG_NAMES[3] = { "tag_glue", "tag_particles", "tag_other" };

struct EthernetPacket
{
  uint8_t version;
  uint16_t length;
  uint16_t timeInterval; /**< unit: 0.1 us */
  uint16_t dotNum;
  uint16_t udpCount;
  uint8_t frameCount;
  uint8_t dataType;
  uint8_t timeType;
  uint8_t rsvd[12];
  uint32_t crc32;
  uint8_t timestamp[8];
  uint8_t data[1]; /**< Point cloud data. */
};

struct CartesianHighRawPoint
{
  int32_t x;            /**< X axis, Unit:mm */
  int32_t y;            /**< Y axis, Unit:mm */
  int32_t z;            /**< Z axis, Unit:mm */
  uint8_t reflectivity; /**< Reflectivity */
  uint8_t tag;          /**< Tag */
};

struct CartesianLowRawPoint
{
  int16_t x;            /**< X axis, Unit:cm */
  int16_t y;            /**< Y axis, Unit:cm */
  int16_t z;            /**< Z axis, Unit:cm */
  uint8_t reflectivity; /**< Reflectivity */
  uint8_t tag;          /**< Tag */
};

struct SpherePoint
{
  uint32_t depth;
  uint16_t theta;
  uint16_t phi;
  uint8_t reflectivity;
  uint8_t tag;
};

enum PointDataType
{
  IMU_DATA = 0x00,
  CARTESIAN_COORDINATE_HIGH = 0x01,
  CARTESIAN_COORDINATE_LOW = 0x02,
  SPHERICAL_COORDINATE = 0x03
};

};
};

#pragma pack(pop)

#endif // SDK2PacketFormat_h
